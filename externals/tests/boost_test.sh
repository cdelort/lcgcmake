#!/bin/sh -e

# look for Boost compiled modules in $1
mods=$(ls $1/ | sed -nE 's/^libboost_([a-z_]*)-.*\.(dylib|so)$/\1/p')

expected_mods="atomic chrono container context coroutine date_time filesystem graph iostreams locale log log_setup prg_exec_monitor program_options python random regex serialization signals system thread timer unit_test_framework wave wserialization"

# check that none of the expected modules is missing
missing_mods=$( (echo $mods ; echo $mods ; echo $expected_mods) | tr ' ' '\n' | sort | uniq -u )
if [ -n "$missing_mods" ] ; then
  echo error: missing modules: $missing_mods
  exit 1
fi
